# ShortShort
## Link shortener written on Django
![Logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/15088694/0zWUzrxsw7k_1_.jpg?width=64)

[ShortShort Web](https://shortshort.pythonanywhere.com/)

[ShortShort Telegram](https://shortshort.pythonanywhere.com/4)

[ShortShort VK](https://shortshort.pythonanywhere.com/5)
